<?php
namespace conu;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WhatTime extends Command {
    protected function configure() {
        $this->setName('cu:whatTime')
            ->setDescription('Display the current time.')
            ->setHelp('Print the current time to STDOUT') //opt
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new \DateTime();
        $output->writeln('It is now ' . $now->format('g:i a'));
    }
}